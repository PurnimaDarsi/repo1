package firstproject;

 class SuperClass {

    public void printMethod() {
        System.out.println("Printed in Superclass.");
    }
  
}
//Here is a subclass, called Subclass, that overrides printMethod():

 public class Subclass extends SuperClass {

    // overrides printMethod in Superclass
    public void printMethod() {
        super.printMethod();
        System.out.println("Printed in Subclass");
    }
    public static void main(String[] args) {
        Subclass s = new Subclass();
        s.printMethod();    
    }
}