package firstproject;

abstract class Animal{
	   //abstract method
	   public abstract void sound();
	   public void type() {
		   System.out.println("It is animal");
	   }
	}
	//Dog class extends Animal class
	public class Dog extends Animal{

	   public void sound(){
		System.out.println("Woof");
	   }
	   public void type() {
		   System.out.println("It is dog");
	   }
	   public static void main(String args[]){
		Animal obj = new Dog();
		obj.sound();
		obj.type();
	   }
	}