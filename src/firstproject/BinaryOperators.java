package firstproject;

import java.util.Scanner;

public class BinaryOperators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 int num1, num2, sum,mul,sub,div,mod;
		 
	        Scanner sc = new Scanner(System.in);
	        System.out.println("Enter First Number: ");
	        num1 = sc.nextInt();
	        
	        System.out.println("Enter Second Number: ");
	        num2 = sc.nextInt();
	        
	        sc.close();
	        sum = num1+num2;
	        mul = num1*num2;
	        div = num1/num2;
	        mod = num1%num2;
	        sub = num1-num2;
	        System.out.println("Sum of these numbers: "+sum);
	        System.out.println("Subraction of these numbers: "+sub);
	        System.out.println("Multiplication of these numbers: "+mul);
	        
	        System.out.println("Division of these numbers: "+div);
	        System.out.println("Modulus of these numbers: "+mod);
	       
	

	}

}
