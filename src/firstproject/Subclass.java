package firstproject;

 class Superclass {
	 Superclass(){
		 System.out.println("Printed in super constructor");
	 }

    public void printMethod() {
        System.out.println("Printed in Superclass.");
    }
}
//Here is a subclass, called Subclass, that overrides printMethod():

 public class Subclass extends Superclass {

    // overrides printMethod in Superclass
    public void printMethod() {
    	//this();
       // super.printMethod();
        System.out.println("Printed in Subclass");
    }
    Subclass(){
    	//this(3);
    	super();
    	System.out.println("Printed in Subclass constructor");
    }
    Subclass(int a){
    	
    }
    public static void main(String[] args) {
        Subclass s = new Subclass();
        
        s.printMethod();    
    }
}
