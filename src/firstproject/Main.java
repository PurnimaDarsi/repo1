package firstproject;

abstract class Base { 
    abstract void fun(); 
    static int a=2;
} 
class Derived extends Base { 
    void fun() { System.out.println("Derived fun() called"); 
    } 
    void ownm() {
    	System.out.println("its derived method"+a);
    }
} 
class Main { 
    public static void main(String args[]) {  
      
        // Uncommenting the following line will cause compiler error as the  
        // line tries to create an instance of abstract class. 
        // Base b = new Base(); 
  
        // We can have references of Base type. 
    	Derived b = new Derived(); 
    	Base.a=5;
        b.fun();  
        b.ownm();
    } 
} 