package firstproject;

public class Grading {
	public static void main(String[] args) {
		int avg=90;
		if(avg>=90) {
			System.out.println("Grade A");
		}else if(avg>=70 && avg<=90) {
			System.out.println("Grade B");
		}else if(avg>=50 && avg<=70) {
			System.out.println("Grade C");
		}else if(avg<=50) {
			System.out.println("Fail");
		}
	}

}
